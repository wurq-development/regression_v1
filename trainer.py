'''
Trainer code for regression
'''
import sys
import os
import numpy as np
import pickle
import torch
import datetime

from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from dataloader import PdataLoader

from utils import error_logger, get_conf_mat, get_pred_plot, save_weight, save_figure

class trainer():
	def __init__(self, params):
		self.params = params

	def train(self, x_train, y_train, x_val, y_val):
		print("Training ", self.params['model'])
		if self.params['model'] == 'LSTM' or 'GRU':
			if x_train.ndim != 3:
				error_logger("RNN only takes dim=3. " +
					"Please check 'extracting_features' is set True, " +
					"or saved files include extracted features. " +
					"Aborted.")
			model = self.train_rnn(x_train, y_train, x_val, y_val)

	def train_rnn(self, x_train, y_train, x_val, y_val):
		from model import LSTM, GRU
		from dataloader import PdataLoader

		epochs = self.params['epochs']
		batch_size = self.params['batch_size']
		seq_length = self.params['seq_length']
		input_dim = 6
		num_layers = self.params['num_layers']
		hidden_dim = self.params['LSTM_feature_size']
		output_dim = 3

		lr = self.params['learning_rate']

		train_loader = DataLoader(PdataLoader(x_train, y_train), batch_size = batch_size, shuffle = True, num_workers = 4)
		val_loader = DataLoader(PdataLoader(x_val, y_val), batch_size = batch_size, shuffle = True, num_workers = 4)
		
		device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
		if self.params['model'] == 'LSTM':
			model = LSTM(input_dim, hidden_dim, batch_size, output_dim, num_layers = num_layers)
		elif self.params['model'] == 'GRU':
			model = GRU(input_dim, hidden_dim, batch_size, output_dim, num_layers = num_layers)

		model = model.to(device)

		criterion = torch.nn.MSELoss() # size_average=False
		check_criterion = torch.nn.L1Loss() # size_average=False
		optimizer = torch.optim.Adam(model.parameters(), lr = lr)#, weight_decay = 0.00001)
		    	
		min_val = 100

		current = datetime.datetime.now()
		weight_dir = self.params['weight_path']
		writer = SummaryWriter('./run')

		for epoch in range(0, epochs):
			total_loss = 0
			total_check_loss = 0

			model.train()

			for i_batch, (sample_batched) in enumerate(train_loader):

				xdata = torch.cat((sample_batched['input_glob_acc'], sample_batched['input_gyro']),dim=2)

				y_pred = model(xdata.float().to(device))
				y_pred_init = torch.empty((y_pred.shape[0],y_pred.shape[1],y_pred.shape[2]), device = device)
				for i in range(y_pred.shape[0]):
					y_pred_init[i,:,:] = y_pred[:,0,:][i,:]
				y_pr_comp = y_pred - y_pred_init

				y_act = sample_batched['output_pos'].float().to(device)
				y_act_init = torch.empty((y_act.shape[0],y_act.shape[1],y_act.shape[2]), device = device)
				for i in range(y_act.shape[0]):
					y_act_init[i,:,:] = y_act[:,0,:][i,:]
				y_act_comp = y_act - y_act_init

				loss = criterion(y_pr_comp, y_act_comp)
				writer.add_scalar("Loss/train", loss, epoch)
				check_loss = check_criterion(y_pr_comp, y_act_comp)
				writer.add_scalar("Loss_M1/train", check_loss, epoch)
				optimizer.zero_grad()
				loss.backward()
				optimizer.step()

				total_loss += loss.item()
				total_check_loss += check_loss.item()

			model.eval()
			with torch.no_grad():
				val_loss = 0
				check_val_loss = 0
				for i_batch, (sample_batched) in enumerate(val_loader):

					xdata = torch.cat((sample_batched['input_glob_acc'], sample_batched['input_gyro']),dim=2)
					y_pred = model(xdata.float().to(device))
					y_pred_init = torch.empty((y_pred.shape[0],y_pred.shape[1],y_pred.shape[2]), device = device)
					for i in range(y_pred.shape[0]):
						y_pred_init[i,:,:] = y_pred[:,0,:][i,:]
					y_pr_comp = y_pred - y_pred_init

					y_act = sample_batched['output_pos'].float().to(device)
					y_act_init = torch.empty((y_act.shape[0],y_act.shape[1],y_act.shape[2]), device = device)
					for i in range(y_act.shape[0]):
						y_act_init[i,:,:] = y_act[:,0,:][i,:]
					y_act_comp = y_act - y_act_init

					loss = criterion(y_pr_comp, y_act_comp)
					check_loss = check_criterion(y_pr_comp, y_act_comp)
					writer.add_scalar("Loss/test", loss, epoch)
					writer.add_scalar("Loss_M1/test", check_loss, epoch)

					val_loss += loss
					check_val_loss += check_loss.item()

				print('[Epoch %d] Train loss: %.4f %.4f \t Val loss: %.4f %.4f' % (epoch + 1, total_loss/i_batch, 
																						   total_check_loss/i_batch, 
																						   val_loss/ i_batch, 
																						   check_val_loss/ i_batch))
				if val_loss/i_batch < min_val:

					if not os.path.exists(weight_dir):
						os.makedirs(weight_dir)

					fname = '{}_w{}_s_{}_epoch_{}_L2_loss_{}.pth'.format(self.params['model'], 
																self.params['seq_length'],
																self.params['stride'],
																str(epoch + 1),
																str(val_loss/i_batch))

					weight_file = os.path.join(weight_dir, fname)
					weight_file2 = os.path.join(weight_dir, 'model_all.pth')

					torch.save(model.state_dict(), weight_file)
					torch.save(model.state_dict(), weight_file2)

					min_val = val_loss/i_batch

		writer.flush()

		return model
