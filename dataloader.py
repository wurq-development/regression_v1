import numpy as np
import torch

class PdataLoader(object):
    def __init__(self, xdata, ydata):
        self.xdata = xdata
        self.ydata = ydata
        self.xgrad = np.zeros((xdata.shape[0], xdata.shape[1], xdata.shape[2]))

    def __getitem__(self, index):

        time = self.xdata[index, :, 0]

        """ Input data """
        input_gyro = self.xdata[index, :, 1:4]
        input_glob_acc = self.xdata[index, :, 4:7]        
        input_loc_acc = self.xdata[index, :, 7:10]
        input_quat = self.xdata[index, :, 10:14]

        """ Output data """
        output_pos = self.ydata[index, :, 1:4]
        output_vel = self.ydata[index, :, 4:7]
        output_acc = self.ydata[index, :, 7:10]
        output_quat = self.ydata[index, :, 10:14]

        cur_sample = {'time': time, 'input_gyro': input_gyro, 'input_glob_acc': input_glob_acc, 'input_loc_acc': input_loc_acc, 'input_quat': input_quat,
                      'output_pos': output_pos, 'output_vel': output_vel, 'output_acc': output_acc, 'output_quat': output_quat}

        return cur_sample
        
    def __len__(self):

        return self.xdata.shape[0]