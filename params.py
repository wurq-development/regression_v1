PARAMS = \
{
	'model': 'GRU', # target model
	'mode': 'test',
	'data_path': './data', 
	'seq_length': 2500,
	'stride': 50,
	'target_train': [1,2,3,4,5,6,7,8,11,12,14],
	'target_val': [13],
	'target_test':[15],
	'temporal': True,
	'save_data': True, # Data saving as npy for effiency
	'save_weight': True, # Save weight
	'save_exit': True,# Save npy file and exit
	'load_data': True, # Loding saved npy
	'saved_datapath': './saved_data', # npy file path
	'weight_path': './weights', # model weight path
	'result_path': './results', # test result path
	'show_conf': True, # Show confusion matrix
	'show_plot': True, # Show test data plot

	#######################
	# PARAMETERS FOR LSTM #	
	'val_split': 0.2,
	'epochs': 500, 
	'batch_size': 128, 
	'learning_rate': 0.0001,
	'LSTM_feature_size': 128, 
	'num_layers': 2,
	'Dense_feature_size': 50
}	
