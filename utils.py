import os, sys
import pickle
import numpy as np
import logging
import importlib
import matplotlib.pyplot as plt
import seaborn as sns

def error_logger(txt):
    logging.error(txt)
    sys.exit()

def load_params():
    if len(sys.argv) != 2:
        error_logger("Include Parameter file.")
    param_path = sys.argv[1]
    if not os.path.isfile(param_path):
        error_logger("Parameter file not found.")
    return importlib.import_module(param_path[:-3]).PARAMS

def save_data(dataset, fname, path):
    if not os.path.exists(path):
        os.makedirs(path)

    for cur_data in dataset.keys():
        np.save(os.path.join(path,
            cur_data + fname + '.npy'), dataset[cur_data])

def get_conf_mat(y_pred, y_gt, labels):
    from sklearn.metrics import confusion_matrix

    cm = confusion_matrix(y_gt, y_pred, normalize = 'true')
    cm = np.round(cm,3)

    conf_label = np.union1d(np.unique(y_pred), np.unique(y_gt))
    conf_label = [x for x in labels if labels.index(x) in conf_label]

    fig, ax = plt.subplots(figsize=(10,8))
    sns.set(font_scale=1.1)
    sns.heatmap(cm, annot=True, fmt='.2f', xticklabels=conf_label, yticklabels=conf_label)
    plt.ylabel('True labels')
    plt.xlabel('Predicted labels')
    plt.title('Confusion matrix')
    
    return fig

def get_pred_plot(y_pred, y_gt, this_labels):

    labels = ['non-exer','BP','BC','LR','SP','LAT','MED','SQ','BL','BR','TRI','DF','DL']

    fig, axs = plt.subplots(figsize=(10, 5))
    plt.plot(y_gt)
    plt.plot(y_pred)
    plt.yticks(range(len(labels)), labels, size = 'small')
    plt.legend(['Ground_truth','Prediction'])

    return fig

def save_weight(model, fname, path):
    if not os.path.exists(path):
        os.makedirs(path)
    if 'SVM' in fname[:5]:
        pickle.dump(model, open(os.path.join(path, fname), 'wb'))
    elif 'LSTM' in fname[:5]:
        pass

def save_figure(figure, fname, path):
    if not os.path.exists(path):
        os.makedirs(path)
    figure.savefig(os.path.join(path, fname + '.png'))
