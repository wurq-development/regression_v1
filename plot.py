import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('results_gru_pos.csv', delimiter=',', dtype=str)
# data_gt = np.loadtxt('trial15_mcl_imu2.csv', delimiter=',', dtype=str)

data = data[1:,:].astype(float)
# data_gt = data_gt[1:,:].astype(float)
# data_gt = data_gt[1000:,:]

print(data.shape)
# print(data_gt.shape)

fig, (ax1, ax2, ax3) = plt.subplots(3, 1,figsize=(20, 10))

ax1.plot(data[:,0])
ax1.plot(data[:,3])

ax2.plot(data[:,1])
ax2.plot(data[:,4])

ax3.plot(data[:,2])
ax3.plot(data[:,5])

ax1.legend(['Prediction','Ground_truth'])
ax1.set_title('vel_x')
ax2.set_title('vel_y')
ax3.set_title('vel_z')

plt.show()
plt.tight_layout()
# fig, axs = plt.subplots(figsize=(20, 5))
# plt.plot(gt_z)
# plt.plot(pred_z)
# # plt.yticks(range(len(labels)), labels, size = 'small')
# plt.legend(['Ground_truth','Prediction'])
# plt.show()


"""

import matplotlib.pylab as plt

x = np.linspace(-np.pi, np.pi, 201)
x = np.sin(x)

idx = 0
x_out = []
seq_length = 10
increment = 1

while True:

	if idx + seq_length > x.shape[0]:
	    break
	x_this = x[idx:idx+seq_length] - x[idx]
	x_out.append(x_this)

	idx += increment

x_out = np.array(x_out)

x_out_new = []

for i in range(x_out.shape[0]):
	if i > 0:
		x_out_new.append(x_out[i,-1] + x_out[i-1,-1])
	else:
		x_out_new.append(x_out[i,-1])

# exit()
x_out_new = np.array(x_out_new)

# x_out_new = np.cumsum(x_out_new)
print(x.shape)
print(x_out.shape)
print(x_out_new.shape)

plt.plot(x[9:])
plt.plot(x_out_new)
plt.legend(['raw','reconstructed'])
# plt.plot(x, np.sin(x))
# plt.xlabel('Angle [rad]')
# plt.ylabel('sin(x)')
plt.axis('tight')
plt.show()	
"""