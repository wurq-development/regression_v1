import numpy as np
import inspect
import os
from scipy.spatial.transform import Rotation as R
from numpy.linalg import inv

class data_processing():
    def __init__(self, params):
        self.params = params
        self.data_path = params['data_path']
        self.seq_length = params['seq_length']
        self.increment = params['stride']
        self.target_test = params['target_test']
        self.dataset = {"xtrain": [], "ytrain": [], 
                        "xval": [], "yval": [], 
                        "xtest": [], "ytest": []}

    def data_to_seq(self, data_imu, data_gt):

        idx = 0
        x_out = []
        y_out = []

        for cfidx in range(len(data_imu)):
            x_raw = np.loadtxt(data_imu[cfidx], delimiter=',', dtype=str)
            y_raw = np.loadtxt(data_gt[cfidx], delimiter=',', dtype=str)
            
            x_raw = x_raw[1:,1:] # time, gyro, glob_acc, loc_acc, quat
            y_raw = y_raw[1:,1:] # time, pos, vel, acc, quat

            x_raw = x_raw.astype(np.float64)
            y_raw = y_raw.astype(np.float64)

            idx = 0

            while True:
                if idx + self.seq_length > x_raw.shape[0]:
                    break
                x_out.append(x_raw[idx:idx+self.seq_length, :])
                y_out.append(y_raw[idx:idx+self.seq_length, :])

                idx += self.increment
                
        return x_out, y_out

    def preprocessing(self):
        print('Data processing.')

        train_imu = [] 
        train_gt = []
        val_imu = [] 
        val_gt = []
        test_imu = []
        test_gt = []      
        
        for trial in self.params['target_train']:
            for imus in [1, 2]:
                train_imu.append(os.path.join(self.data_path, 'trial' + str(trial) + '_imu' + str(imus) + '.csv'))
                train_gt.append(os.path.join(self.data_path, 'trial' + str(trial) + '_mcl_imu' + str(imus) + '.csv'))
        for trial in self.params['target_val']:
            for imus in [1,2]:
                val_imu.append(os.path.join(self.data_path, 'trial' + str(trial) + '_imu' + str(imus) + '.csv'))
                val_gt.append(os.path.join(self.data_path, 'trial' + str(trial) + '_mcl_imu' + str(imus) + '.csv'))
        for trial in self.params['target_test']:
            for imus in [2]:
                test_imu.append(os.path.join(self.data_path, 'trial' + str(trial) + '_imu' + str(imus) + '.csv'))
                test_gt.append(os.path.join(self.data_path, 'trial' + str(trial) + '_mcl_imu' + str(imus) + '.csv'))

        """ train/val dataset"""
        x_train, y_train = self.data_to_seq(train_imu, train_gt)
        x_val, y_val = self.data_to_seq(val_imu, val_gt)

        """ test dataset """
        self.increment = 1
        x_test, y_test = self.data_to_seq(test_imu, test_gt)

        self.dataset['xtrain'] = np.array(x_train)
        self.dataset['ytrain'] = np.array(y_train)
        self.dataset['xval'] = np.array(x_val)
        self.dataset['yval'] = np.array(y_val)
        self.dataset['xtest'] = np.array(x_test)
        self.dataset['ytest'] = np.array(y_test)

        return self.dataset

