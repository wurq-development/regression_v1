import os, sys
import numpy as np
from data_processing import data_processing as dpr
from trainer import trainer
from test import test
from utils import load_params, save_data

'''
2022. 1. 13 by Haedo Cho (hcho@g.harvard.edu)
*** Things to implement ***

'''

def main():
    params = load_params()
    if params['load_data']:
        fname = '_{}_w{}_s{}'.format(params['model'], 
        params['seq_length'], params['stride'])
        
        if params['mode'] == 'train':
            xtrain = np.load(os.path.join(params['saved_datapath'],
                'xtrain' + fname +'.npy'))
            ytrain = np.load(os.path.join(params['saved_datapath'],
                'ytrain'+ fname +'.npy'))
            xval = np.load(os.path.join(params['saved_datapath'],
                'xval' + fname +'.npy'))
            yval = np.load(os.path.join(params['saved_datapath'],
                'yval'+ fname +'.npy'))
        elif params['mode'] == 'test':
            xtest = np.load(os.path.join(params['saved_datapath'],
                'xtest' + fname +'.npy'))
            ytest = np.load(os.path.join(params['saved_datapath'],
                'ytest' + fname +'.npy'))
        
        print('Data loaded.')

    else:
        proc = dpr(params)
        dataset = proc.preprocessing()
        xtrain, ytrain = dataset['xtrain'], dataset['ytrain']
        xval, yval = dataset['xval'], dataset['yval']
        xtest, ytest = dataset['xtest'], dataset['ytest']
        print('Finished data processing.')

        if params['save_data']:
            fname = '_{}_w{}_s{}'.format(params['model'], 
            params['seq_length'], params['stride'])
            save_data(dataset, fname, params['saved_datapath'])
            print('Data saved.')
        if params['save_exit']:
            exit()

    if params['mode'] == 'train':
        tr = trainer(params)
        model = tr.train(xtrain, ytrain, xval, yval)
    elif params['mode'] == 'test':
        test_model = test(params)
        test_model.test(xtest,ytest)

if __name__ == '__main__':
    main()


