import torch
import torch.nn as nn

class LSTM(nn.Module):
    def __init__(self, input_dim, hidden_dim, batch_size, output_dim, num_layers):
        super(LSTM, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.batch_size = batch_size
        self.num_layers = num_layers

        self.lstm = nn.LSTM(input_dim, hidden_dim, num_layers, batch_first = True, bidirectional = True)
        self.lstm2 = nn.LSTM(hidden_dim * 2, hidden_dim, num_layers, batch_first = True , bidirectional = True)
        self.lstm3 = nn.LSTM(hidden_dim * 2, hidden_dim, num_layers, batch_first = True , bidirectional = True)

        self.relu = nn.ReLU(inplace=True)

        self.linear = nn.Linear(hidden_dim * 2, 100)
        self.linear2 = nn.Linear(100,50)
        self.linear3 = nn.Linear(50, output_dim)

    def forward(self, input): 
        lstm_out, self.hidden = self.lstm(input)
        # lstm_out = self.lstm2(lstm_out)
        # lstm_out = self.lstm3(lstm_out)

        y_pred = self.linear(lstm_out)
        y_pred = self.relu(y_pred)
        y_pred = self.linear2(y_pred)
        y_pred = self.relu(y_pred)
        y_pred = self.linear3(y_pred)

        return y_pred

class GRU(nn.Module):
    def __init__(self, input_dim, hidden_dim, batch_size, output_dim, num_layers):
        super(GRU, self).__init__()
        self.input_dim = input_dim
        self.hidden_dim = hidden_dim
        self.batch_size = batch_size
        self.num_layers = num_layers

        self.gru = nn.GRU(input_dim, hidden_dim, num_layers, batch_first = True, bidirectional = True)
        self.gru2 = nn.GRU(hidden_dim * 2, hidden_dim, num_layers, batch_first = True , bidirectional = True)
        self.gru3 = nn.GRU(hidden_dim * 2, hidden_dim, num_layers, batch_first = True , bidirectional = True)

        self.relu = nn.ReLU(inplace=True)

        self.linear = nn.Linear(hidden_dim * 2, 100)
        self.linear2 = nn.Linear(100, output_dim)
        # self.linear3 = nn.Linear(50, output_dim)

    def forward(self, input): 
        gru_out, self.hidden = self.gru(input)
        # lstm_out = self.lstm2(lstm_out)
        # lstm_out = self.lstm3(lstm_out)

        y_pred = self.linear(gru_out)
        y_pred = self.relu(y_pred)
        y_pred = self.linear2(y_pred)
        # y_pred = self.relu(y_pred)
        # y_pred = self.linear3(y_pred)

        return y_pred