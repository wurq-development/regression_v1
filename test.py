'''
Test code for regression
'''
import sys
import os
import numpy as np
import pickle
import torch
import datetime

from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
from dataloader import PdataLoader

from utils import error_logger, get_conf_mat, get_pred_plot, save_weight, save_figure

class test():
	def __init__(self, params):
		self.params = params

	def test(self, x_test, y_test):
		print("Testing ", self.params['model'])
		if self.params['model'] == 'LSTM' or 'GRU':
			if x_test.ndim != 3:
				error_logger("RNN only takes dim=3. " +
					"Please check 'extracting_features' is set True, " +
					"or saved files include extracted features. " +
					"Aborted.")
			self.test_rnn(x_test, y_test)

	def test_rnn(self, x_test, y_test):
		from model import LSTM, GRU
		from dataloader import PdataLoader

		epochs = self.params['epochs']
		batch_size = self.params['batch_size']
		seq_length = self.params['seq_length']
		input_dim = 6
		num_layers = self.params['num_layers']
		hidden_dim = self.params['LSTM_feature_size']
		output_dim = 3

		test_loader = DataLoader(PdataLoader(x_test, y_test), batch_size = 1, shuffle = False, num_workers = 1)
		
		device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

		if self.params['model'] == 'LSTM':
			model = LSTM(input_dim, hidden_dim, batch_size, output_dim, num_layers = num_layers)
		elif self.params['model'] == 'GRU':
			model = GRU(input_dim, hidden_dim, batch_size, output_dim, num_layers = num_layers)
		
		model = model.to(device)
		model.load_state_dict(torch.load(os.path.join(self.params['weight_path'], 'model_all.pth')))
		model.eval()
		
		criterion = torch.nn.MSELoss() # size_average=False
		check_criterion = torch.nn.L1Loss() # size_average=False
		writer = SummaryWriter('./run')

		with torch.no_grad():
			val_loss = 0
			check_val_loss = 0
			res = np.empty((0, 12), float)

			for i_batch, (sample_batched) in enumerate(test_loader):

				time = sample_batched['time'].float().to(device)

				xdata = torch.cat((sample_batched['input_loc_acc'], sample_batched['input_gyro']),dim=2)

				y_pred = model(xdata.float().to(device))
				y_pred_init = torch.empty((y_pred.shape[0],y_pred.shape[1],y_pred.shape[2]), device = device)
				for i in range(y_pred.shape[0]):
					y_pred_init[i,:,:] = y_pred[:,0,:][i,:]
				y_pr_comp = y_pred - y_pred_init

				y_act = sample_batched['output_pos'].float().to(device)
				y_act_init = torch.empty((y_act.shape[0],y_act.shape[1],y_act.shape[2]), device = device)
				for i in range(y_act.shape[0]):
					y_act_init[i,:,:] = y_act[:,0,:][i,:]
				y_act_comp = y_act - y_act_init

				loss = criterion(y_pr_comp, y_act_comp)
				writer.add_scalar("Loss/train", loss)
				check_loss = check_criterion(y_pr_comp, y_act_comp)
				writer.add_scalar("Loss_M1/train", check_loss)

				cur_res = np.hstack([y_pr_comp[:,-1,:].detach().cpu().numpy(),
							     	 y_act_comp[:,-1,:].detach().cpu().numpy(),
							     	 y_pred[:,-1,:].detach().cpu().numpy(),
							     	 y_act[:,-1,:].detach().cpu().numpy()])

				res = np.append(res,cur_res, axis=0)

				val_loss += loss
				check_val_loss += check_loss.item()

			print(res.shape)
			print('Val loss: %.3f %.3f' % (val_loss/ i_batch, check_val_loss/ i_batch))
		header = "pred_vel_x, pred_vel_y, pred_vel_z, gt_vel_x, gt_vel_y, gt_vel_z, pred_vel_x, pred_vel_y, pred_vel_z, gt_vel_x, gt_vel_y, gt_vel_z"
		np.savetxt('results.csv', np.array(res), delimiter=",", header= header)

	
